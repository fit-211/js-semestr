import { get_latest_titles, get_random, search_titles } from "./search.mjs"
import { weather } from "./weather.mjs"


const hello_container = document.getElementById('hello_container')
const main_container = document.getElementById('main_container');
const hello_textfield = document.getElementById('hello_textfield');
const next_btn = document.getElementById('next_btn');

//hello page
next_btn.addEventListener('click', async (event) => {
    event.preventDefault();
    if (hello_textfield.value !== '') {

        const hello_header = document.createElement('h3');
        hello_header.id = 'hello_header';
        hello_header.textContent = `Добро пожаловать, ${hello_textfield.value}!`;
        main_container.appendChild(hello_header);

        hello_container.remove();

        const titles = await get_latest_titles();
        display_titles(titles);

        add_search();

        add_random_btn();

    }
    else input_err.hidden = false;
});

hello_textfield.addEventListener('keydown', function (event) {
    if (event.key === 'Enter') {
        next_btn.click();
    }
});

// main page
function display_titles(titles) {
    const header = document.createElement('h3');
    header.id = 'main_header';
    header.textContent = 'Новинки:'
    const title_container = document.createElement('div');
    title_container.appendChild(header);
    title_container.id = 'title_container';
    main_container.appendChild(title_container);

    for (const title of titles) {
        const title_div = document.createElement('div');
        title_div.classList.add('title_div');
        title_container.appendChild(title_div);

        const poster_div = document.createElement('div');
        poster_div.classList.add('poster_div');
        title_div.appendChild(poster_div);

        const poster_img = document.createElement('img');
        poster_img.src = title.poster;
        poster_img.alt = `${title.name} poster`;
        poster_img.classList.add('poster_img');
        poster_img.draggable = false;
        poster_div.appendChild(poster_img);

        const info_div = document.createElement('div');
        info_div.classList.add('info_div');
        title_div.appendChild(info_div);

        const name_h3 = document.createElement('h3');
        name_h3.textContent = title.name;
        info_div.appendChild(name_h3);

        const desc_p = document.createElement('p');
        desc_p.textContent = title.description;
        info_div.appendChild(desc_p);

        const year_p = document.createElement('p');
        year_p.textContent = `Год выхода: ${title.year}`;
        poster_div.appendChild(year_p);

        const series_count_p = document.createElement('p');
        if (title.series_count !== null)
            series_count_p.textContent = `Количество серий: ${title.series_count}`;
        else series_count_p.textContent = `Количество серий: неизвестно`;
        poster_div.appendChild(series_count_p);

        const genres_p = document.createElement('p');
        genres_p.textContent = `Жанры: ${title.genres}`;
        poster_div.appendChild(genres_p);
    }
}



function add_search() {
    const search_container = document.createElement('div');
    search_container.classList.add('search_container');
    main_container.insertBefore(search_container, title_container);

    const search_input = document.createElement('input');
    search_input.type = 'text';
    search_input.placeholder = 'Введите название аниме...';
    search_input.classList.add('search_input');
    search_input.maxLength = '90'
    search_container.appendChild(search_input);

    const search_btn = document.createElement('button');
    search_btn.textContent = 'Найти';
    search_btn.classList.add('search_btn');
    search_container.appendChild(search_btn);

    search_btn.addEventListener('click', async () => {
        if (!document.querySelector(".result_container")) {
            const query = search_input.value;

            const titles = await search_titles(query);
            show_search_list(titles, query)

        }
        else {
            document.querySelector(".result_container").remove();
            search_btn.click();
        }
    });

    search_input.addEventListener('keyup', async (event) => {
        if (event.key === 'Enter') {
            if (!document.querySelector(".result_container")) {
                const query = search_input.value;

                const titles = await search_titles(query);
                show_search_list(titles, query)

            }
            else {
                document.querySelector(".result_container").remove();
                search_btn.click();
            }
        }
    });
}

function show_search_list(titles, query) {
    if (!document.querySelector(".not_found_container") && titles.length === 0) {
        show_search_not_found(query);
        return;
    }
    else if (document.querySelector(".not_found_container") && titles.length === 0) {
        document.querySelector(".not_found_container").remove();
        show_search_not_found(query);
        return;
    }

    else if (document.querySelector(".not_found_container") && titles.length > 0)
        document.querySelector(".not_found_container").remove();

    const result_container = document.createElement('div');
    result_container.classList.add('result_container');
    main_container.insertBefore(result_container, title_container)


    titles.forEach(title => {
        const title_div = document.createElement('div');
        title_div.classList.add('s_title_div');
        result_container.appendChild(title_div);

        const poster_img = document.createElement('img');
        poster_img.src = title.poster;
        poster_img.alt = `${title.name} poster`;
        poster_img.classList.add('s_poster_img');
        title_div.appendChild(poster_img);

        const s_info = document.createElement('div');
        s_info.classList.add('s_info');
        title_div.appendChild(s_info);

        const name_p = document.createElement('p');
        name_p.classList.add('name_p');
        name_p.textContent = title.name;
        s_info.appendChild(name_p);

        const genres = document.createElement('p');
        genres.textContent = title.genres;
        genres.classList.add('genres');
        s_info.appendChild(genres);

        title_div.addEventListener('click', () => {
            result_container.remove();

            document.querySelector("#title_container").style.display = 'none';

            document.getElementsByClassName('search_input')[0].value = '';

            let search_textfield = document.querySelector('.search_input');
            let search_btn = document.querySelector('.search_btn');
            search_textfield.disabled = true;
            search_btn.disabled = true;


            const detail_container = document.createElement('div');
            detail_container.classList.add('detail_container');
            main_container.appendChild(detail_container);

            const poster_img = document.createElement('img');
            poster_img.src = title.poster;
            poster_img.alt = `${title.name} poster`;
            poster_img.draggable = false;
            poster_img.classList.add('poster_img');
            detail_container.appendChild(poster_img);

            const info_block = document.createElement('div');
            info_block.classList.add('info_block');
            detail_container.appendChild(info_block);


            const name_h3 = document.createElement('h3');
            name_h3.textContent = title.name;
            info_block.appendChild(name_h3);

            const desc_p = document.createElement('p');
            desc_p.textContent = title.description;
            info_block.appendChild(desc_p);

            const year_p = document.createElement('p');
            year_p.textContent = `Год выхода: ${title.year}`;
            info_block.appendChild(year_p);

            const series_count_p = document.createElement('p');
            if (title.series_count !== null)
                series_count_p.textContent = `Количество серий: ${title.series_count}`;
            else series_count_p.textContent = `Количество серий: неизвестно`;
            info_block.appendChild(series_count_p);

            const genres_p = document.createElement('p');
            genres_p.textContent = `Жанры: ${title.genres}`;
            info_block.appendChild(genres_p);

            const close_btn = document.createElement('button')
            close_btn.textContent = 'X';
            close_btn.classList.add('close_btn');
            detail_container.appendChild(close_btn);

            close_btn.addEventListener('click', () => {
                detail_container.remove();
                search_textfield.disabled = false;
                search_btn.disabled = false;
                document.querySelector("#title_container").style.display = '';
            });
            document.addEventListener('keydown', function (event) {
                if (event.key === 'Escape') {
                    close_btn.click();
                }
            });
        });
    });
}

function show_search_not_found(query) {
    const not_found_container = document.createElement('div');
    not_found_container.classList.add('not_found_container');

    const not_found_mes = document.createElement('p');
    not_found_mes.textContent = `По запросу "${query}" ничего не найдено...`;
    not_found_container.appendChild(not_found_mes);
    main_container.insertBefore(not_found_container, title_container);
}

function add_random_btn() {
    const random_btn = document.createElement('button');
    random_btn.textContent = 'Случайное аниме';
    random_btn.classList.add('random_btn');

    let search_container = document.querySelector('.search_container');
    search_container.appendChild(random_btn);

    random_btn.addEventListener('click', async () => {
        random_btn.disabled = true;

        const div = document.createElement('div');
        if (!document.querySelector('#loading_container')) {
            div.id = 'loading_container';
            div.innerHTML = `<image src="./media/loading.png" width=25% height=25% id='loading_image'></image>`
            main_container.insertBefore(div, title_container);
        }

        const random_title = await get_random();

        div.remove();

        if (document.querySelector('.result_container'))
        document.querySelector('.result_container').remove();
        if (document.querySelector('.not_found_container'))
            document.querySelector('.not_found_container').remove();
        document.querySelector("#title_container").style.display = 'none';

        document.getElementsByClassName('search_input')[0].value = '';

        let search_textfield = document.querySelector('.search_input');
        let search_btn = document.querySelector('.search_btn');
        search_textfield.disabled = true;
        search_btn.disabled = true;

        if (document.querySelector('.detail_container'))
            document.querySelector('.detail_container').remove();

        const detail_container = document.createElement('div');
        detail_container.classList.add('detail_container');
        main_container.appendChild(detail_container);

        const poster_img = document.createElement('img');
        poster_img.src = random_title.poster;
        poster_img.alt = `${random_title.name} poster`;
        poster_img.draggable = false;
        poster_img.classList.add('poster_img');
        detail_container.appendChild(poster_img);

        const info_block = document.createElement('div');
        info_block.classList.add('info_block');
        detail_container.appendChild(info_block);

//описание
        const name_h3 = document.createElement('h3');
        name_h3.textContent = random_title.name;
        info_block.appendChild(name_h3);

        const desc_p = document.createElement('p');
        desc_p.textContent = random_title.description;
        info_block.appendChild(desc_p);

        const year_p = document.createElement('p');
        year_p.textContent = `Год выхода: ${random_title.year}`;
        info_block.appendChild(year_p);

        const series_count_p = document.createElement('p');
        if (random_title.series_count !== null)
            series_count_p.textContent = `Количество серий: ${random_title.series_count}`;
        else series_count_p.textContent = `Количество серий: неизвестно`;
        info_block.appendChild(series_count_p);

        const genres_p = document.createElement('p');
        genres_p.textContent = `Жанры: ${random_title.genres}`;
        info_block.appendChild(genres_p);


        const close_btn = document.createElement('button')
        close_btn.textContent = 'X';
        close_btn.classList.add('close_btn');
        detail_container.appendChild(close_btn);

        close_btn.addEventListener('click', () => {
            random_btn.disabled = false;
            detail_container.remove();
            search_textfield.disabled = false;
            search_btn.disabled = false;
            document.querySelector("#title_container").style.display = '';
        });
        document.addEventListener('keydown', function (event) {
            if (event.key === 'Escape') {
                close_btn.click();
            }
        });
    });
}

window.onload = () => {
    weather()
}
