async function weather() {
    navigator.geolocation.getCurrentPosition(success, error, {
        enableHighAccuracy: true
      })
      
      function success({ coords }) {
        const { latitude, longitude } = coords
        const position = [latitude, longitude]
        console.log(position)
        const apiKey = '7cc3ab334ccb2e2189534e4a61f65261';
        const url=`https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${apiKey}&units=metric&lang=ru`

      fetch(url)
        .then(response => response.json())
        .then(data => {
            console.log(data)
            const airtext = document.querySelector('.airtext')
            const humiditytext = document.querySelector('.humiditytext')
            const realfeeltext = document.querySelector('.realfeeltext')
            const main_temp = document.querySelector('.main')
            const main_sub = document.querySelector('.mainsub')
            const aqitext = document.querySelector('.aqitext')
            airtext.innerHTML = `Ветер \n${Math.round(data.wind.speed)}м/с`
            realfeeltext.innerHTML = `Ощущается, как \n${Math.round(data.main.feels_like)}°C`
            main_temp.innerHTML = `${Math.round(data.main.temp)}°C`
            humiditytext.innerHTML = `Давление \n${data.main.humidity} мбар`
            main_sub.innerHTML = `${data.name}`
            aqitext.innerHTML = `${data.visibility}м`
        })
        .catch(error => console.error(error))

        if (latitude == NULL){
            weather()
        }
        
      }

      console.log(data)
      
      function error({ message }) {
        console.log(message)
      }
      setTimeout(weather(), 180000);
  }

  export {weather}